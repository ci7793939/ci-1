# ci-1

# https://gitlab.com/filador-public

## Les Jobs
Les jobs servent à exécuter chaque stage dans pipeline
Un pipeline est un ensemble de stage

## Les Stages
Les stages permettent de definir l'ordre d'execution d'un pipeline

## Build Docker
L'utilisation d'image docker pour builder une artifact présente plusieurs avantages:
- Permet de nous affranchir du besoin d'installer les outils/depandances/librairies (JDk, node) de build directement sur la VM du runner
- Evite la saturation de la memoire du runner
- Les jobs sont executé par le runner, mais dans une image docker, ce qui permet de produire facilement plusieurs types d'artifacts (Java, node)

## Release
- La reelease permet de produire  une version/tag de notre artifact en le poussant dans une registry docker/nexus

## dind (docker in docker)
- Représente un couche de securité entre le conteneur de build et le deamon docker lors du build
 service:
  - docker:dind
